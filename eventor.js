'use strict';

function Eventor() {
    //Code here.
}

//-----------NO EDITING BELOW THIS POINT-------------------------

var Distilled = window.Distilled;
var assert = window.assert;

var suite = new Distilled();

/**
 * Your eventor should start with three methods:
 * - `on(string event, function callback)`
 * - `trigger(string event)`
 * - `off(function callback)`
 */
suite.test('API', function (suite) {
    var bus = new Eventor();
    suite.test('`bus.on`', typeof bus.on === 'function');
    suite.test('`bus.trigger`', typeof bus.trigger === 'function');
    suite.test('`bus.off`', typeof bus.off === 'function');
});

/** Testing functionality */
suite.test('Basic events', function () {
    var bus = new Eventor();

    /**
     * - `on` takes a string event and a callback.
     * - `trigger` takes a string.
     * - When `trigger` is invoked, the relevant callback should also be invoked.
     */
    var result = false;
    bus.on('event', function () {
        result = true;
    });

    bus.trigger('event');
    return result;
});

/** Advanced functionality */
suite.test('Wildcards (*)', function () {

    /**
     * Calling `on` with a wildcard (*), will attach an event that
     * will get called no matter what is triggered.
     */
    var bus = new Eventor();
    var result = false;
    bus.on('*', function () {
        result = true;
    });

    bus.trigger('event' + Math.random());
    return result;

});

/**
 * Listeners should optionally be allowed to consume data about events. This
 * should be passed as an optional parameter in the callback in `on`. Data can
 * be (again, optionally) passed into `trigger` after the event.
 */
suite.test('Passing data to events', function () {

    var bus = new Eventor();
    var key = Math.random();

    bus.on('event', function (data) {
        assert.ok(data[key]);
    });

    bus.trigger('event', {
        [key] : true
    });
});

/**
 * Users need a way to attach and remove temporary event listeners. Add a new
 * method, `off` that allows you to remove a listener that you passed in
 * earlier.
 */
suite.test('Removing listeners: `off` method', function (suite) {
    var bus = new Eventor();

    var callback = function () {
        throw new Error('`listener was invoked after being removed`');
    };

    bus.on('event', callback);
    bus.off(callback);
    bus.trigger('event');
});

//---------------GOTCHAS-------------------------------

var gotchas = suite.test('gotchas');

gotchas.test('`trigger` can be called on events that have no listeners', function () {
    var bus = new Eventor();
    bus.trigger('undefined' + Math.random());
});

gotchas.test('calling `off` removes *all* instances of the passed in callback', function () {
    var bus = new Eventor();
    var callback = function () {
        throw new Error('listener was invoked after being removed');
    };

    bus.on('event', callback);
    bus.on('event2', callback);

    bus.off(callback);
    bus.trigger('event');
    bus.trigger('event2');
});

gotchas.test('Eventor is not a singleton', function () {
    var bus = new Eventor();
    var bus2 = new Eventor();

    bus.on('event', function () {});
    bus2.on('event', function () {
        throw new Error('listener should not have been invoked');
    });

    bus.trigger('event');
});

//------------------BONUS------------------------------

var bonus = suite.test('bonus');

bonus.test('no exposed private variables', function () {
    var bus = new Eventor();

    var accepted = ['on', 'trigger', 'off'];
    var keys = Object.keys(bus);
    keys.forEach(function (key) {
        assert.ok(accepted.includes(key));
    });
});

/**
 * It's cumbersome for quick, anonymous methods to have to be stored separately
 * and passed around when removing event listeners. Some users might prefer to
 * have a method returned from `on` that allows them to remove the listener
 * without storing a reference.
 */
bonus.test('removing listeners via a returned method', function () {
    var bus = new Eventor();

    var off = bus.on('event', function () {
        throw new Error('`listener was invoked after being removed`');
    });

    off();
    bus.trigger('event');
});
