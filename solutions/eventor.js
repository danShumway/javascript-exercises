function Eventor () {
    var events = {};

    return {
        on : function (event, callback) {
            events[event] = events[event] || [];
            events[event].push(callback);

            //Allow user to remove callback via returned method.
            var that = this;
            return function () {
                that.off(callback);
            };
        },

        trigger : function (event, data) {
            var listeners = events[event] || [];
            listeners = listeners.concat(events['*'] || []); // Wildcard events

            listeners.forEach(function (listener) {
                listener(data);
            });
        },

        off : function (callback) {
            Object.keys(events).forEach(function (key) { //This could be better than O(n).
                events[key] = events[key].filter(function (listener) {
                    return listener !== callback;
                });
            });
        }
    };
}
